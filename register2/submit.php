<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$sex = "";
$facebook = "";
$facultate = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$error = 0;
$error_text = "";

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['facultate'])){
	$facultate = $_POST['facultate'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}


if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($facebook) || empty($birth) || empty($department) || empty($question) || empty($captcha_inserted) || empty($check) || empty($facultate)){
	$error = 1;
	$error_text = "One or more fields are empty!";
}

if(strlen($facultate) < 3 || strlen($facultate) > 30){
	$error = 1;
	$error_text = "Facultate is shorter or longer than expected!";
}

if (!ctype_alpha($facultate)) {
	$error = 1;
	$error_text = "Facultate is not valid!";
}

if(strlen($firstname) < 3 || strlen($firstname) > 20 || strlen($lastname) < 3 || strlen($lastname) > 20){
	$error = 1;
	$error_text = "First or Last name is shorter or longer than expected!";
}

if (strlen($question) < 15) {
	$error =1;
	$error_text = "Question is shorter than expected!";
}

if (!(ctype_alpha($firstname) && ctype_alpha($lastname))) {
	$error = 1;
	$error_text = "First or Last name is not valid!";
}

if(!is_numeric($phone) || strlen($phone)!=10){
	$error = 1;
	$error_text = "Phone number is not valid!";
}

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $error = 1;
    $error_text ="Email adress is not valid!";
}

if (!is_numeric($cnp) || strlen($cnp)!=13 || $cnp[0] < 1 || $cnp[0] > 6) {
	$error =1;
	$error_text ="CNP is not valid!";
}

if (!filter_var($facebook, FILTER_VALIDATE_URL)) {
	$error = 1;
	$error_text ="Facebook link is not valid!";
}

if ($captcha_inserted != $captcha_generated) {
	$error =1;
	$error_text = "Captcha incorrect!";
}

$date2=date("d-m-Y");//today's date

   $date1=new DateTime($birth);
   $date2=new DateTime($date2);

   $interval = $date1->diff($date2);

   $myage= $interval->y; 

  if ($myage < 18 || $myage > 100) {
  	$error = 1;
  	$error_text = "Age is not valid!";
  }

if ($cnp[0] == 1 || $cnp[0] == 3 || $cnp[0] == 5) {
	$sex = "M";
}
elseif ($cnp[0] == 2 || $cnp[0] == 4 || $cnp[0] == 6) {
	$sex = "F";
}

if ($birth[2] != $cnp[1] || $birth[3] != $cnp[2] || $birth[5] != $cnp[3] || $birth[6] != $cnp[4] || $birth[8] != $cnp[5] || $birth[9] != $cnp[6]) {
	$error = 1;
	$error_text = "Birth and CNP don't match!";
}

try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}

$query = $con->prepare( "SELECT `email` FROM `register2` WHERE `email` = ?" );
$query->bindValue( 1, $email );
$query->execute();
if($query->rowCount() != 0){
	$error = 1;
	$error_text = "Email already used!";
}

$querypart = $con->prepare( "SELECT * FROM `register2`");
$querypart->execute();
if($querypart->rowCount() >=50){
	$error = 1;
	$error_text = "Too many participants(only 50 allowed)";
}



if($error == 0){

$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,sex,facebook,facultate,birth,department,question) VALUES(:firstname,:lastname,:phone,:email,:cnp,:sex,:facebook,:facultate,:birth,:department,:question)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':sex',$sex);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':facultate',$facultate);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

}else{
    echo "Succes";}
}else{
	echo $error_text;
	return;
}
